package org.example.test;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;

import org.example.pages.CalculatorPage;

import java.time.Duration;

public class TestBase {

    protected CalculatorPage calc;
    protected WebDriver driver;

    @BeforeClass
    void setUp() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        String URL = "https://web2.0calc.com";
        driver.get(URL);

        WebElement consentButton = driver.findElement(By.className("fc-button-label"));
        consentButton.click();
    }

    @BeforeMethod
    public void initPageObjects() {
        calc = PageFactory.initElements(driver, CalculatorPage.class);
    }


    @AfterMethod
    void clear() {
        driver.findElement(By.id("BtnClear")).click();
        driver.findElement(By.id("trigodeg")).click();

    }

    @AfterClass
    void teardown() {
        driver.quit();
    }
}
