package org.example.test;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCalculatorKeyboardInputs extends TestBase {

    @Test (groups = { "keys" })
    void testOperatorPrecedenceByKeyboardInput() {

        calc.performCalculationByKeyboardInput("35*999+(100/4)");
        String result = calc.getInputValue();

        Assert.assertEquals(result, "34990");
    }


    @Test (groups = { "keys" })
    void testCosByKeyboardInput() {

        calc.enableRadBtn();
        calc.performCalculationByKeyboardInput("cos(pi)");

        String result = calc.getInputValue();

        Assert.assertEquals(result, "-1");
    }


    @Test (groups = { "keys" })
    void testSquareRootKeyboardInput() {

        calc.performCalculationByKeyboardInput("sqrt(81)");

        String result = calc.getInputValue();

        Assert.assertEquals(result, "9");
    }


    @Test (dependsOnGroups = { "keys" })
    public void testHistory() {

        calc.clickHistoryBtn();
        int result = calc.countOperations();

        Assert.assertEquals(result, 3);
    }
}
