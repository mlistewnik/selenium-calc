package org.example.test;
import org.testng.Assert;
import org.testng.annotations.Test;



public class TestCalculatorClicking extends TestBase {


    @Test (groups = { "clicks" })
    void testOperatorPrecedenceByClicking() {
        calc.performMultipleOperations();
        String result = calc.getInputValue();

        Assert.assertEquals(result, "34990");
    }




    @Test (groups = { "clicks" })
    void testCosByClicking() {
        calc.enableRadBtn();
        calc.calculateCosPi();
        String result = calc.getInputValue();

        Assert.assertEquals(result, "-1");

    }



    @Test (groups = { "clicks" })
    public void testSquareRootByClicking() {
        calc.calculateSquareRoot();
        String result = calc.getInputValue();

        Assert.assertEquals(result, "9");
    }




    @Test (dependsOnGroups = { "clicks" })
    public void testHistory() {
        calc.clickHistoryBtn();
        int result = calc.countOperations();

        Assert.assertEquals(result, 3);
    }
}