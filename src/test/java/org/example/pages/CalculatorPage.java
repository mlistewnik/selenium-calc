package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class CalculatorPage  {

    private final WebDriver driver;


    @FindBy(id = "input")
    @CacheLookup
    private WebElement inputWindow;

    @FindBy(id = "Btn0")
    @CacheLookup
    private WebElement zeroBtn;

    @FindBy(id = "Btn1")
    @CacheLookup
    private WebElement oneBtn;

    @FindBy(id = "Btn3")
    @CacheLookup
    private WebElement threeBtn;

    @FindBy(id = "Btn4")
    @CacheLookup
    private WebElement fourBtn;

    @FindBy(id = "Btn5")
    @CacheLookup
    private WebElement fiveBtn;

    @FindBy(id = "Btn8")
    @CacheLookup
    private WebElement eightBtn;

    @FindBy(id = "Btn9")
    @CacheLookup
    private WebElement nineBtn;

    @FindBy(id = "BtnCalc")
    @CacheLookup
    private WebElement equalsBtn;

    @FindBy(id = "BtnPlus")
    @CacheLookup
    private WebElement plusBtn;


    @FindBy(id = "BtnMult")
    @CacheLookup
    private WebElement multBtn;

    @FindBy(id = "BtnDiv")
    @CacheLookup
    private WebElement divBtn;

    @FindBy(id = "BtnParanL")
    @CacheLookup
    private WebElement leftParBtn;

    @FindBy(id = "BtnParanR")
    @CacheLookup
    private WebElement rightParBtn;

    @FindBy(id = "BtnCos")
    @CacheLookup
    private WebElement cosBtn;

    @FindBy(id = "BtnPi")
    @CacheLookup
    private WebElement piBtn;

    @FindBy(id = "trigorad")
    @CacheLookup
    private WebElement radBtn;

    @FindBy(id = "BtnSqrt")
    @CacheLookup
    private WebElement sqrtBtn;

    @FindBy(css = "#hist > button.btn.dropdown-toggle.pull-right")
    @CacheLookup
    private WebElement historyDropdownBtn;

    static final String  firstOperationToVerify = "35*999+(100/4)";
    static final String secondOperationToVerify = "cos(pi)";
    static final String thirdOperationToVerify = "sqrt(81)";
    static final String resultInHistory = "#hist > button.last.btn";
    static final String historicalRecords = "#histframe > ul > li > p.l";


    public CalculatorPage(WebDriver driver) {
        this.driver = driver;
    }

    public void performCalculationByKeyboardInput(String input){

        inputWindow.sendKeys(input);
        confirmOperationFromKeyboard();
        new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.textToBe(By.cssSelector(resultInHistory), input));
    }

    public void confirmOperationFromKeyboard(){
        inputWindow.sendKeys(Keys.ENTER);


    }


    public void performMultipleOperations() {
        threeBtn.click();
        fiveBtn.click();
        multBtn.click();
        int i;
        for (i = 0; i <= 2; i++) {
            nineBtn.click();
        }
        plusBtn.click();
        leftParBtn.click();
        oneBtn.click();
        int k;
        for (k = 0; k <= 1; k++) {
            zeroBtn.click();
        }
        divBtn.click();
       fourBtn.click();
       rightParBtn.click();
        clickEqualsBtn();
        new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.textToBe(By.cssSelector(resultInHistory), firstOperationToVerify));
    }

    public void enableRadBtn() {
        radBtn.click();
    }


    public void calculateCosPi() {
        cosBtn.click();
        piBtn.click();
        rightParBtn.click();
        clickEqualsBtn();
        new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.textToBe(By.cssSelector(resultInHistory), secondOperationToVerify));
    }

    public void calculateSquareRoot() {
        sqrtBtn.click();
        eightBtn.click();
        oneBtn.click();
        rightParBtn.click();
        clickEqualsBtn();
        new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.textToBe(By.cssSelector(resultInHistory), thirdOperationToVerify));
    }


    public String getInputValue() {
        return inputWindow.getAttribute("value");
    }


    public void clickEqualsBtn() {
        equalsBtn.click();
    }

    public int countOperations() {
        int counter = 0;
        List<WebElement> allHistory = driver.findElements(By.cssSelector(historicalRecords));
        for (WebElement e : allHistory) {

            if (e.getText().equals(CalculatorPage.firstOperationToVerify)) {
                counter++;
            }
            if (e.getText().equals(CalculatorPage.secondOperationToVerify)) {
                counter++;
            }
            if (e.getText().equals(CalculatorPage.thirdOperationToVerify)) {
                counter++;
            }
        }
        return counter;
    }


    public void clickHistoryBtn() {
        historyDropdownBtn.click();
    }
}
