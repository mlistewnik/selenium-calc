# Calc project #


This projects contains two sets of tests running operations on an online calculator (https://web2.0calc.com/):
* Set 1: Operations performed using calculator buttons
* Set 2: Operations performed with keyboard input

The tests are configured to run in parallel.

To run the tests, please clone the repo and run `mvn clean test` from the root directory.


### Basic stack ###

* Java 17
* Maven  3.8.1
* Selenium Webdriver 4.0.0
* TestNG 7.4.0

